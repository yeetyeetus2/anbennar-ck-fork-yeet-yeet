﻿#########################################################
#														#
#  DO NOT LEAVE GAPS IN "index" OR THE GAME WILL CRASH  #
#														#
#########################################################

interaction_category_diplomacy = {
	index = 0
	desc = "DIPLOMACY_ACTIONS"
}

interaction_category_friendly = {
	index = 1
	desc = "FRIENDLY_ACTIONS"
}

interaction_category_hostile = {
	index = 2
	desc = "HOSTILE_ACTIONS"
}

interaction_category_vassal = {
	index = 3
	desc = "VASSAL_ACTIONS"
}

interaction_category_prison = {
	index = 4
	desc = "IMPRISOMENT_ACTIONS"
}

interaction_category_religion = {
	index = 5
	desc = "RELIGIOUS_ACTIONS"
}

interaction_category_spells = { # Anbennar
	index = 6
	desc = "SPELLS_ACTIONS"
}

interaction_category_uncategorized = {
	index = 7 # Anbennar: was 6
	default = yes
	desc = "UNCATEGORIZED_ACTIONS"
}

interaction_debug_main = {
	index = 8 # Anbennar: was 7
	desc =  "DEBUG_MAIN"
}