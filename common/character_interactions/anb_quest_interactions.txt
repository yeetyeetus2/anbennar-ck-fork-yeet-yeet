﻿view_quest_interaction = {
	category = interaction_category_uncategorized
	special_interaction = feudalize_tribal_holding

	desc = view_quest_interaction_desc

	target_type = title
	target_filter = recipient_domain_titles
	
	auto_accept = yes
	
	can_be_picked_title = {
		scope:target = {
			tier = tier_county
		}
	}

	has_valid_target_showing_failures_only = {
		scope:target = {
			tier = tier_county
			county_has_quest = yes
		}
	}

	on_accept = {
		# Fire the manager
		view_quest_interaction_effect = yes
	}

	ai_will_do = {
		base = 1500
	}
}
